"use strict";

/*
1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
*/

for (const par of document.getElementsByTagName("p")) {
  par.style.backgroundColor = "#ff0000";
}

/*
2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. 
	Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
*/

const optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentElement);
const childNodes = optionsList.childNodes;
childNodes.forEach((node) => {
  console.log(node.nodeName, node.nodeType);
});

/* 
3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
This is a paragraph
*/
/* На сторінці немає елемента з класом testParagraph, але є елемент з таким id*/

document.querySelector("#testParagraph").innerText = "This is a paragraph";

/* 
4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
*/

const mainHeaderChildren = document.querySelector(".main-header").children;
for (const child of mainHeaderChildren) {
  console.log(child);
  child.classList.add("nav-item");
}

/*
5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
*/
document.querySelectorAll(".section-title").forEach((element) => {
  element.classList.remove("section-title");
});
